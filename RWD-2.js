function toggleNavMenu() {
    var navbar = document.getElementById("navbar");
    if (navbar.className === "nav-expand") {
        navbar.className = "nav-normal";
    } else {
        navbar.className = "nav-expand";
    }
}