# Basic CSS Study
I've followed the advices of [Web Developer Roadmap 2018](https://github.com/kamranahmedse/developer-roadmap). This repo is just what I practice and apply from the example codes of [w3schools](https://www.w3schools.com), and  also include some details that I've learned from another. 

# Grid vs Flexbox
- **1D** (column or row) use `flexbox`
- **2D** (column and row) use `grid`

# Media Queries: screen vs only screen vs nothing
- `screen` prevent another media type such as `print` for printer
- `only screen` prevent browser doesn't support css3


# `<section>` vs `<article>` 
from [w3schools](https://www.w3schools.com/html/html5_semantic_elements.asp)

> You will also find pages with `<section>` elements containing `<section>` elements, and `<article>` elements containing `<article>` elements.


# Tip
- must use `box-sizing: border-box` in css for use actually specific `width` and `height` of elements.
```css
* {
    box-sizing: border-box;
}
```
- must use `overflow: hidden` in parent of `float` because...
> ...the wrapper container **doesn’t** expand to the height of the child floating elements. -- [webdesignerwall.com](http://webdesignerwall.com/tutorials/css-clearing-floats-with-overflow)

> ...`overflow: hidden` will give you a new block formatting context. -- [makandracards.com](https://makandracards.com/makandra/9245-use-overflow-hidden-to-avoid-floating-elements-from-wrapping-a-container-s-text)

# References
- [CSS Naming Conventions that Will Save You Hours of Debugging](https://medium.freecodecamp.org/css-naming-conventions-that-will-save-you-hours-of-debugging-35cea737d849)
- [CSS Flexbox](https://www.w3schools.com/css/css3_flexbox.asp)
- [CSS Grid Layout Module](https://www.w3schools.com/css/css_grid.asp)
- [Relationship of grid layout to other layout methods](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Relationship_of_Grid_Layout)
- [CSS box-sizing Property](https://www.w3schools.com/cssref/css3_pr_box-sizing.asp)
- [CSS Templates](https://www.w3schools.com/css/css_templates.asp)
- [HTML5 Semantic Elements](https://www.w3schools.com/html/html5_semantic_elements.asp)
- [Standard HTML5 Semantic Layout](https://gist.github.com/thomd/9220049)
- [semantic html](https://internetingishard.com/html-and-css/semantic-html/)
- [What is the purpose of the “role” attribute in HTML?](https://stackoverflow.com/questions/10403138/what-is-the-purpose-of-the-role-attribute-in-html)
- [What is the difference between “screen” and “only screen” in media queries?](https://stackoverflow.com/questions/8549529/what-is-the-difference-between-screen-and-only-screen-in-media-queries)
- [CSS Units Best Practices](https://gist.github.com/basham/2175a16ab7c60ce8e001)
- [Responsive Topnav with Dropdown](https://www.w3schools.com/howto/howto_js_responsive_navbar_dropdown.asp)
- [CSS: Clearing Floats with Overflow](http://webdesignerwall.com/tutorials/css-clearing-floats-with-overflow))
- [Use "overflow: hidden" to avoid floating elements from wrapping a container's text](https://makandracards.com/makandra/9245-use-overflow-hidden-to-avoid-floating-elements-from-wrapping-a-container-s-text)